# SLK to CSV

Tool for converting Microsoft Symbolic Link (SYLK) files (typically having a
`.slk` suffix) to other formats.

More information on this format can be found at the following locations:

- [Wikipedia](https://en.wikipedia.org/wiki/Symbolic_Link_(SYLK))
- Outflank.nl
  - [Format study](https://www.outflank.nl/blog/2019/10/30/abusing-the-sylk-file-format/)
  - [Format specification](https://outflank.nl/upload/sylksum.txt)
