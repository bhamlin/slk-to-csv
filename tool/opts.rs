use clap::Parser;

#[derive(Debug, Parser)]
pub struct CommandLineOptions {
    #[clap(default_value = "-")]
    pub file_name: String,
    #[clap(short = 'l', long = "rust-log", env, default_value = "info")]
    pub rust_log: String,
}

pub fn parse_args() -> CommandLineOptions {
    CommandLineOptions::parse()
}
