// #![allow(unreachable_patterns, unused_variables)]

mod opts;

use env_logger::{Builder, Env};
use log::debug;
use slklib::format::SlkData;
use std::{
    fs,
    io::{BufReader, BufWriter, Write},
};

type Whatevs = anyhow::Result<()>;

fn main() -> Whatevs {
    let opts = opts::parse_args();
    Builder::from_env(Env::default().default_filter_or(opts.rust_log)).init();

    let file_handle = fs::File::open("temp/data.slk")?;
    let file_reader = BufReader::new(file_handle);
    let slk_data = SlkData::load_from_reader(file_reader)?;

    debug!("Size: {} x {}", slk_data.cols, slk_data.rows);
    let csv_data = slk_data.as_csv_rows();

    if opts.file_name.eq("-") {
        for line in csv_data {
            println!("{line}");
        }
    } else {
        let output = fs::File::create(opts.file_name)?;
        let mut writer = BufWriter::new(output);

        for line in csv_data {
            writeln!(writer, "{line}")?;
        }
    }

    Ok(())
}
