use std::fmt::Display;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum SlkRecordParseError {
    UnknownRecordType(String),
    UnableToParse,
    RecordTooLong,
}

impl Display for SlkRecordParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::UnknownRecordType(id) => format!("Unknown record type: {}", id.clone()),
            Self::UnableToParse => "Unable to parse input line".into(),
            Self::RecordTooLong => "Record length exceeded 350 characters".into(),
        };

        write!(f, "{}", message)
    }
}
