use std::{
    cmp::max,
    collections::HashMap,
    io::{BufRead, Read},
};

use log::{debug, info, trace};

use crate::{
    error::SlkRecordParseError,
    rows::{
        data::{cell::CellData, format::FormatData},
        LocationData, RecordType,
    },
};

#[derive(Debug)]
pub struct SlkData {
    pub cols: usize,
    pub rows: usize,
    pub data_map: HashMap<usize, HashMap<usize, String>>,
}

impl SlkData {
    pub fn load_from_reader<R: BufRead>(mut reader: R) -> Result<Self, SlkRecordParseError> {
        let mut location = LocationData::default();
        let mut col_max = 0;
        let mut row_max = 0;
        let mut data_map: HashMap<usize, HashMap<usize, String>> = Default::default();

        let mut line = String::new();
        while let Ok(line_length) = reader.read_line(&mut line) {
            if let Some((text, _)) = line.split_once('\n') {
                debug!(target:"slk-parser", "got line: {text}");
                let record = line.as_str().try_into()?;
                match record {
                    RecordType::Format(FormatData::CellData(ref loc)) => {
                        location.copy_from(loc);
                    }
                    RecordType::Cell(CellData::Value(text)) => {
                        if location.is_set() {
                            let col = location.x.unwrap() as usize;
                            let row = location.y.unwrap() as usize;
                            data_map.entry(row).or_default().entry(col).or_insert(text);

                            col_max = max(col, col_max);
                            row_max = max(row, row_max);
                        }
                    }
                    _ => (),
                }
            } else {
                break;
            }
            line.clear();
        }

        Ok(SlkData {
            cols: col_max,
            rows: row_max,
            data_map,
        })
    }

    pub fn as_csv_rows(&self) -> Vec<String> {
        let rows = self.rows;
        let cols = self.cols;

        let mut csv_data = Vec::<String>::with_capacity(rows);
        let mut values = Vec::<String>::with_capacity(cols);
        for row in 1..=rows {
            for col in 1..=cols {
                if let Some(row_data) = self.data_map.get(&row) {
                    values.push(row_data.get(&col).cloned().unwrap_or("".into()));
                }
            }

            trace!("{values:?}");
            let string = values
                .iter()
                .map(|input| {
                    if input.contains(',') {
                        format!("\"{input}\"")
                    } else {
                        input.clone()
                    }
                })
                .collect::<Vec<_>>()
                .join(",");
            csv_data.push(string);
            values.clear();
        }

        csv_data
    }
}
