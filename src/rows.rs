pub mod data;

use log::{debug, trace};

use self::data::{cell::CellData, format::FormatData, id::IdData, picture::PictureData};
use crate::{
    error::SlkRecordParseError,
    tools::{find_starts_with, tokenize},
};

#[derive(Debug)]
pub enum RecordType {
    Id(IdData),           // ID
    Bounds(LocationData), // B
    Cell(CellData),       // C
    Picture(PictureData), // P
    Format(FormatData),   // F
    // FileName,   // NU
    // Names,      // NN
    // Options,    // O
    // External,   // NE
    // Window,     // W
    // Chart,      // NL
    // End,        // E
    Noop,
}

impl TryFrom<&str> for RecordType {
    type Error = SlkRecordParseError;

    fn try_from(_value: &str) -> Result<Self, Self::Error> {
        match _value.split_once(';') {
            Some((id, data)) => Self::decode_by_id(id, data),
            None => Ok(Self::Noop),
        }
    }
}

impl RecordType {
    pub fn decode_by_id(id: &str, data: &str) -> Result<RecordType, SlkRecordParseError> {
        debug!(target:"record-parser", "got id: {id}");
        match id {
            "ID" => Ok(Self::Id(IdData::parse(data)?)),
            "P" => Ok(Self::Picture(PictureData::parse(data)?)),
            "B" => Ok(Self::Bounds(LocationData::parse(data)?)),
            "F" => Ok(Self::Format(FormatData::parse(data)?)),
            "C" => Ok(Self::Cell(CellData::parse(data)?)),
            _ => Err(SlkRecordParseError::UnknownRecordType(id.to_string())),
        }
    }
}

pub trait SlkDataParser {
    fn parse(data: &str) -> Result<Self, SlkRecordParseError>
    where
        Self: Sized;
}

#[derive(Debug, Default)]
pub struct LocationData {
    pub x: Option<u64>,
    pub y: Option<u64>,
}

impl SlkDataParser for LocationData {
    fn parse(data: &str) -> Result<Self, SlkRecordParseError>
    where
        Self: Sized,
    {
        let mut parts = tokenize(data)?;
        let y = find_starts_with(&parts, 'Y').and_then(|seq| seq.parse().ok());
        let x = find_starts_with(&parts, 'X').and_then(|seq| seq.parse().ok());

        Ok(LocationData { x, y })
    }
}

impl LocationData {
    pub fn is_set(&self) -> bool {
        self.y.is_some() && self.x.is_some()
    }

    pub fn copy_from(&mut self, source: &LocationData) {
        self.y = source.y;
        self.x = source.x;
    }
}
