use log::trace;

use crate::error::SlkRecordParseError;

pub fn find_starts_with(parts: &[String], ch: char) -> Option<&str> {
    trace!(target:"slk-tools", "find '{ch}' in: {parts:?}");
    for part in parts {
        if part.starts_with(ch) {
            return Some(&part[1..]);
        }
    }

    None
}

/// Tokenize on semi-colons. Doesn't yet, but will eventually also allow for
/// semi-colons within double quotes.
///
/// In the event of any parse failure, bails on entire parse.
pub fn tokenize(input: &str) -> Result<Vec<String>, SlkRecordParseError> {
    // Buffer size of 512 as String is sized by characters and not bytes.
    let mut buffer = String::with_capacity(512);
    let mut parts = vec![];
    // let mut quoted_section = false;

    for ch in input.chars() {
        if parts.len() > 350 {
            // println!("ERR: {parts:?}");
            return Err(SlkRecordParseError::RecordTooLong);
        }
        // if buffer.len() > 350 {
        //     println!("ERR: {buffer:?}");
        //     return Err(RecordParseError::RecordTooLong);
        // }

        match ch {
            // '"' => {
            //     quoted_section = !quoted_section;
            // }
            '\n' | '\r' => {
                if !buffer.is_empty() {
                    parts.push(buffer.clone());
                    buffer.clear();
                }
                // break;
                // quoted_section = false;
            }
            ';' => {
                // if quoted_section {
                //     buffer.push(ch);
                // } else
                if !buffer.is_empty() {
                    parts.push(buffer.clone());
                    buffer.clear();
                }
            }
            _ => {
                buffer.push(ch);
            }
        };
    }

    Ok(parts)
}
