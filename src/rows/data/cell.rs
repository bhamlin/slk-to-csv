use log::{debug, trace};

use crate::{
    rows::SlkDataParser,
    tools::{find_starts_with, tokenize},
};

#[derive(Debug)]
pub enum CellData {
    Empty,
    Value(String),
}

impl SlkDataParser for CellData {
    fn parse(data: &str) -> Result<Self, crate::error::SlkRecordParseError>
    where
        Self: Sized,
    {
        let parts = tokenize(data)?;
        debug!(target:"record-parser-cell", "got data: {parts:?}");
        let value = find_starts_with(&parts, 'K').map(|seq| {
            let n = seq.len();

            if n > 1 {
                seq[1..n - 1].to_string()
            } else {
                String::with_capacity(0)
            }
        });

        if let Some(text) = value {
            Ok(CellData::Value(text))
        } else {
            Ok(CellData::Empty)
        }
    }
}
