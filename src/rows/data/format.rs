use log::debug;

use crate::{
    rows::{LocationData, SlkDataParser},
    tools::{find_starts_with, tokenize},
};

#[derive(Debug)]
pub enum FormatData {
    Misc,
    CellData(LocationData),
}

impl SlkDataParser for FormatData {
    fn parse(data: &str) -> Result<Self, crate::error::SlkRecordParseError>
    where
        Self: Sized,
    {
        let parts = tokenize(data)?;
        debug!(target:"record-parser-format", "got data: {parts:?}");
        let y = find_starts_with(&parts, 'Y').and_then(|seq| seq.parse().ok());
        let x = find_starts_with(&parts, 'X').and_then(|seq| seq.parse().ok());

        if y.is_some() || x.is_some() {
            Ok(FormatData::CellData(LocationData { x, y }))
        } else {
            Ok(FormatData::Misc)
        }
    }
}
