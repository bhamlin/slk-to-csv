use crate::rows::SlkDataParser;

#[derive(Debug)]
pub struct PictureData {}

impl SlkDataParser for PictureData {
    fn parse(data: &str) -> Result<Self, crate::error::SlkRecordParseError>
    where
        Self: Sized,
    {
        Ok(PictureData {})
    }
}
