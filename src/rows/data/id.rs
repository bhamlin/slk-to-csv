use log::debug;

use crate::{
    error::SlkRecordParseError,
    rows::SlkDataParser,
    tools::{find_starts_with, tokenize},
};

#[derive(Debug, Default)]
pub struct IdData {
    pub generator: Option<String>,
    // pub n_cell_protection: bool,
    // pub external_refs: bool,
}

impl SlkDataParser for IdData {
    fn parse(data: &str) -> Result<Self, SlkRecordParseError>
    where
        Self: Sized,
    {
        let parts = tokenize(data)?;
        debug!(target:"record-parser-id", "got data: {parts:?}");
        let generator = find_starts_with(&parts, 'P').map(|seq| seq.to_string());

        Ok(IdData { generator })
    }
}
